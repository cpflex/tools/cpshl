import { getUrl } from "./benttools";

const yaml = require("js-yaml");
const { DEFAULT_SCHEMA } = require("js-yaml");

const DEFAULT_PROTOCOL = "http";
const DEFAULT_DOMAIN = "global.cpflex.tech";
const API_CLAIMS = "claims/v1.0";
const API_AUTH = "auth/v1.0";
const API_ADMIN = "admin/v1.0";
const API_PROVISIONING = "provisioning/v1.0";
const API_HUB = "hub/v1.0";
const API_INTEGRATION = "integration/v1.0";

export default async function createApp(options) {
  const user = _getUser(options);
  const pwd = _getPassword(options);

  if (!user || !pwd) {
    throw new Error("User name or password is not defined.");
  }
  // Login into Console Admin.
  var myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/json");

  var raw = JSON.stringify({
    username: user,
    password: pwd,
  });

  var requestOptions = {
    method: "POST",
    headers: myHeaders,
    body: raw,
    redirect: "follow",
  };

  //{idUser, jwt}
  const response = await fetch(_getUrl(API_AUTH, "/login"), requestOptions);

  return (app = {
    getUserId: () => response.idUser,
    getJwt: () => response.jwt,
    stripQuotes: _stripQuotes,
    getUrl: _getUrl,
    getApiKey: () => _getApiKey(options),
  });

  /****************************************************************************
   * Internal Implementation
   ****************************************************************************/

  function _stripQuotes(str) {
    if (str == null || str.length == 0) return null;
    else return str.replace(/^"|"$/g, "");
  }

  function _getUrl(api, path = "") {
    let domain = DEFAULT_DOMAIN;
    if (process.env.CPFLEX_DOMAIN) {
      domain = _stripQuotes(process.env.CPFLEX_DOMAIN);
    }
    return `${DEFAULT_PROTOCOL}://${domain}/${api}${path}`;
  }

  function _getApiKey(options) {
    if (options.apikey === undefined) {
      if (
        process.env.CPFLEX_APIKEY === undefined ||
        process.env.CPFLEX_APIKEY.length == 0
      )
        return null;
      else return _stripQuotes(process.env.CPFLEX_APIKEY);
    } else return options.apikey;
  }

  /**
   * Returns the workspace login user
   * @param {*} options
   * @returns {string} The workspace user if defined.
   */
  function _getUser(options) {
    if (options.user === undefined) {
      if (
        process.env.CPFLEX_USER === undefined ||
        process.env.CPFLEX_USER.length == 0
      )
        return null;
      else return _stripQuotes(process.env.CPFLEX_USER);
    } else {
      return options.user;
    }
  }

  /**
   * Returns the workspace login password
   * @param {*} options
   * @returns {string} The workspace user password if defined.
   */
  function _getPassword(options) {
    if (options.pwd === undefined) {
      if (
        process.env.CPFLEX_USER === undefined ||
        process.env.CPFLEX_USER.length == 0
      )
        return null;
      else return _stripQuotes(process.env.CPFLEX_USER);
    } else {
      return options.pwd;
    }
  }
}
