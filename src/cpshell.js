"use strict";
/**
 *  Name:  cpshell.h
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2024 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

const { program } = require("commander");
const repl = require("repl");
const getTerminalEvaluator = require("./shellEvaluator");
const replEvaluator = getTerminalEvaluator();

try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------

  program
    .version("1.0.0")
    .description(
      "CP-Flex Command Shell. Provides CLI for working with entities associated with console workspaces.  If command is not defined, the application enters interactive shell mode.",
      {
        command: "Optional command",
        arguments: "Optional command arguments",
      }
    )
    .arguments("[command] [arguments...]")
    .option("-u,  --user <name>", "Console user login name.")
    .option("-p,  --pwd <password>", "Console user login password.")
    .option(
      "-w, --wkspc <workspace>",
      "Defines the initial workspace.  If not defined, last opened workspace is used."
    )
    .option(
      "-a, --active <account>",
      "The initial active account reference.  If not define workspace default active account is used."
    )
    .option(
      "-f,  --file <filename>",
      "Command file (.cpshl) comprising one or more CP-Flex Command Shell commands."
    )

    //------------------------------------------------------------------------
    // Command Implementation
    //------------------------------------------------------------------------
    .action(async function (/*command, arguments*/) {
      if (command) {
        //todo.
        throw new Error("command argument not currently supported");
      }

      // Login into Console Admin.
      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");

      var raw = JSON.stringify({
        username: "testuser1",
        password: "GoodDay1!",
      });

      var requestOptions = {
        method: "POST",
        headers: myHeaders,
        body: raw,
        redirect: "follow",
      };

      fetch("http://staging.cpflex.tech/auth/v1.0/login", requestOptions)
        .then((response) => response.text())
        .then((result) => console.log(result))
        .catch((error) => console.log("error", error));

      return repl.start({
        prompt: "cpshell>",
        eval: (cmd, context, filename, callback) => {
          const outputStr = replEvaluator(cmd);

          console.log(outputStr);

          callback();
        },
      });
    });

  program.parse(process.argv);
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
