# CP-Flex&trade; Shell Command Line Tool.

Node JS based CLI provide an easy-to-use command line interface for interacting all of CP-Flex APIs.  Provides persistent workspaces using admin tools API and authentication for all of CP-Flex APIs including hub, translation, integration, claims, admin-tools, and provisioning.

## Installation and Setup

### Install Node.js  

The tools require Node.js (javascript) tools to install your system.   If not 
already installed, please find the package for your 
system an install it. Go to https://nodejs.org/en/download/ and select your OS.
Follow the instructions there.

### Clone and Install cpclaim
The tools here are essentially scripts that can be packaged and deployed 
globally on your system. This provides a command line interface for working with 
CP-Flex claim registrar APIs.   

1. Install Git.  The best way to obtain the tools is to use GIT this can be 
   found at https://git-scm.com/downloads 

1. Clone this project to your local computer. Open a command window or 
   terminal, change to a working directory, where you want to clone the source files 
   (e.g. .../projects/cpflex/) and execute the following comand.  
    ```
    clone https://gitlab.com/cpflex/tools/cpclaim.git
    ```

1. Once cloned, change directory into the cpacc project and install the the package locally
    ```
    npm install
    ```
    For developers, the source code is freely available to see how to use the APIs.   

1. To access the tools from anywhere on your machine, package them as a single executable. 
   Install pkg to create a single binary and then use the tool to create executables for 
   Windows, MacOS, or linux.
    ```
    npm install -g pkg
    pkg .
    ```
    This will create cpclaim-win.exe, cpclaim-linux, and cpclaim-macos.

1. Select the package for you OS and copy it to a folder that is on your path in order to 
   execute globally.  For example, copy the *cphub-win.exe* to a tools folder (e.g. `C:/bin`) 
   and rename the file, *cpclaim.exe*.

1. Once installed, change your working directory to a project folder and execute the following command:
    ``` 
    > cpclaim
    Usage: cpclaim [options] [command]

    Options:
        -V, --version                      output the version number
        -h, --help                         display help for command

    Commands:
        registrar                          Claim registration commands
        registrar claim <acct> <claimkey>  Claims an unclaimed device
        registrar list <acct>              Lists claimed devices for the specified account
        registrar info <acct> <claimkey>   Retrieves device information given account and a serial number or claimkey
        help [command]                     display help for command
    ```
    If successful, the main help menu should be shown as above.


1.  Set up you environment variables with your account apikey and access key:  
    * On Windows:<br/>
        ```
        set CPFLEX_APIKEY="<YOUR API KEY>"
        ```

    * On Linux or similar systems
        ```
        declare -x CPFLEX_APIKEY='<YOUR API KEY>"
        ```
    This environment variable can be made permanent by storing in your system configuration. 
    You can also add these on the command line (the '-K' option) 
    if you are changing API KEYS often or don't want them to persist.   


---
*Copyright &copy; 2020 Codepoint Technologies, Inc.*<br/>
*All Rights Reserved*

